export class Teams{
    success: number;
    result: Info[];
}

export class Info{
    team_key: string;
    team_name: string;
    team_logo: string;
    players: PlayersTeam[];
    coaches: Coaches[];
}

export class PlayersTeam{
    player_key: number;
    player_name: string;
    player_number: string;
    player_country: string;
    player_type: string;
    player_age: string;
    player_match_played: string;
    player_goals: string;
    player_yellow_cards: string;
    player_red_cards: string;
}

export class Coaches{
    coach_name: string;
    coach_country: string;
    coach_age: string;
}