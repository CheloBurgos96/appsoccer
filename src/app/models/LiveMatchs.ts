export class Match{
    succes: boolean;
    result: StatisticsLive[];
}

export class StatisticsLive{
    event_key: string;
    event_date: string;
    event_time: string;
    event_home_team: string;
    event_away_team: string;
    event_halftime_result: string;
    event_final_result: string;
    event_penalty_result: string;
    event_status: string;
    league_name: string;
    league_season: string;
    event_stadium: string;
    home_team_logo: string;
    away_team_logo: string;
    event_home_formation: string;
    event_away_formation: string;
    goalscorers: GoalsScorers[];
    cards: PlayerCards[];
}

export class GoalsScorers{
    time: string;
    home_scorer: string;
    score: string;
    away_scorer: string;
}

export class PlayerCards{
    time: string;
    home_fault: string;
    card: string;
}