export class League{
    succes: boolean;
    result: Leagues[];
}

export class Leagues{
    league_key: string;
    league_name: string;
    country_key: string;
    country_name: string;
    league_logo: string;
}