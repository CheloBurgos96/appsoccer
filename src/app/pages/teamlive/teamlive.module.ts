import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TeamlivePageRoutingModule } from './teamlive-routing.module';

import { TeamlivePage } from './teamlive.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TeamlivePageRoutingModule
  ],
  declarations: [TeamlivePage]
})
export class TeamlivePageModule {}
