import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamlivePage } from './teamlive.page';

const routes: Routes = [
  {
    path: '',
    component: TeamlivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamlivePageRoutingModule {}
