import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TeamsResultPageRoutingModule } from './teams-result-routing.module';

import { TeamsResultPage } from './teams-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TeamsResultPageRoutingModule
  ],
  declarations: [TeamsResultPage]
})
export class TeamsResultPageModule {}
