import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamsResultPage } from './teams-result.page';

const routes: Routes = [
  {
    path: '',
    component: TeamsResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamsResultPageRoutingModule {}
