import { Component, OnInit } from '@angular/core';
import { Teams } from '../../models/TeamsLeague';
import { DataTeamsService } from '../../services/data-teams.service';
@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  team: Teams;
  constructor(private teamService: DataTeamsService) { }

  ngOnInit() {
    this.teamService.getTeams().subscribe(data =>{
      //console.log(data);
      this.team = data;
    })
    //this.teams = this.teamService.getTeams();
    console.log(this.team);
  }

}
