import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeaminforPage } from './teaminfor.page';

const routes: Routes = [
  {
    path: '',
    component: TeaminforPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeaminforPageRoutingModule {}
