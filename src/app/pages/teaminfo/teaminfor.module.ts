import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TeaminforPageRoutingModule } from './teaminfor-routing.module';

import { TeaminforPage } from './teaminfor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TeaminforPageRoutingModule
  ],
  declarations: [TeaminforPage]
})
export class TeaminforPageModule {}
