import { TestBed } from '@angular/core/testing';

import { DataTeamsService } from './data-teams.service';

describe('DataTeamsService', () => {
  let service: DataTeamsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataTeamsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
