import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Info, Teams} from '../models/TeamsLeague';
import {PlayersTeam} from '../models/TeamsLeague';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataTeamsService {
  url: string = "https://allsportsapi.com/api/football/?&met=Teams&teamId=2616&APIkey=27314664a2954ee312f8c067df4c5ca7e6388772a17d39c8532c02693036545f"
  
  constructor(private http: HttpClient) { }

  getTeams(): Observable<Teams>{
    return this.http.get<Teams>(this.url);
    //return this.team;
  }
}
