import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [TeamsListComponent],
  exports: [TeamsListComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
