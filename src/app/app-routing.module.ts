import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./pages/main/main.module').then( m => m.MainPageModule)
  },
  {
    path: 'teamlive',
    loadChildren: () => import('./pages/teamlive/teamlive.module').then( m => m.TeamlivePageModule)
  },
  {
    path: 'teaminfor',
    loadChildren: () => import('./pages/teaminfo/teaminfor.module').then( m => m.TeaminforPageModule)
  },
  {
    path: 'teams-result',
    loadChildren: () => import('./pages/teams-result/teams-result.module').then( m => m.TeamsResultPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
